package com.example.demo.controllers;

import com.example.demo.entities.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

@Controller
public class ErrorController {

    @GetMapping("/403")
    public String error403(@ModelAttribute User user, ModelMap modelMap)
    {
        modelMap.addAttribute("user", user);
        return "/error/403";
    }


    @GetMapping("/404")
    public String error404(@ModelAttribute User user, ModelMap modelMap)
    {
        modelMap.addAttribute("user", user);
        return "/error/403";
    }

    @GetMapping("/500")
    public String error500(@ModelAttribute User user, ModelMap modelMap)
    {
        modelMap.addAttribute("user", user);
        return "/error/403";
    }
}
