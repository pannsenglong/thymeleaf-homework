package com.example.demo.controllers;

import com.example.demo.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;


@Controller
public class AdminDashboardController
{
    UserRepository userRepository;

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    //HOME Page
    @GetMapping("/home")
    public String goToHomePage()
    {
        return "/layouts/home-layout";
    }


    //ARTICLES
    @GetMapping("/admin/article")
    public String goToArticlePage()
    {
        return "/layouts/article-layout";
    }


    @GetMapping("/admin/review-article")
    public String goToArticleReviewPage()
    {
        return "/layouts/article-review-layout";
    }

    @GetMapping("/admin/dashboard")
    public String goToDashboard()
    {
        return "/layouts/dashboard-layout";
    }

}
