package com.example.demo.security;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableWebSecurity
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter
{

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.csrf().disable()
                .authorizeRequests()
                .antMatchers(HttpMethod.GET, "/admin/review-article").hasRole("REVIEWER")
                .antMatchers(HttpMethod.GET, "/admin/article").hasRole("EDITOR")
                .antMatchers(HttpMethod.GET, "/admin/dashboard").hasRole("ADMIN")
                .anyRequest().authenticated()
                .and()

                .formLogin()
                .loginPage("/admin/login")
                .permitAll()

                .defaultSuccessUrl("/home",true)
                .and().logout()
                .logoutUrl("/admin/logout")
                .logoutSuccessUrl("/admin/login")
                .and()
                .exceptionHandling().accessDeniedPage("/403");
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("dara").password("{noop}dara123").roles("USER").and()
                .withUser("kanha").password("{noop}kanha123").roles("USER","EDITOR").and()
                .withUser("reksmey").password("{noop}reksmey123").roles("USER","REVIEWER").and()
                .withUser("makara").password("{noop}makara123").roles( "USER","EDITOR","REVIEWER","ADMIN");
    }


}
